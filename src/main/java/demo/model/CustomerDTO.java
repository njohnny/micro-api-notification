package demo.model;

public class CustomerDTO {
	
	private Long id;
	
	private String name;
	
	private String email;
	
	private String fiscalNumber;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFiscalNumber() {
		return fiscalNumber;
	}

	public void setFiscalNumber(String fiscalNumber) {
		this.fiscalNumber = fiscalNumber;
	}

	@Override
	public String toString() {
		return "CustomerDTO [name=" + name + ", email=" + email + ", fiscalNumber=" + fiscalNumber + "]";
	}
	
	

}
