package demo.entities;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "TB_NOTIFICATION_ORDERS")
public class NotificationEntity  {   

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private Long id;
	
	@Column(name = "orderId")
	private Long orderId;
	
	
	@Column(name="delivery_dt", nullable=false)
	@JsonFormat(pattern="dd/MM/yyyy hh:mm")
	private  LocalDateTime deliveryDateTime;
	
	@Column(name="customer_id", nullable=false)
	private Long customer;
	
	@Column(name="customer_email", nullable=false)
	private String customerEmail;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public LocalDateTime getDeliveryDateTime() {
		return deliveryDateTime;
	}

	public void setDeliveryDateTime(LocalDateTime autorizationDateTime) {
		this.deliveryDateTime = autorizationDateTime;
	}
	
	public Long getCustomerId() {
		return customer;
	}

	public Long getCustomer() {
		return customer;
	}

	public void setCustomer(Long customer) {
		this.customer = customer;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	@Override
	public String toString() {
		return "NotificationEntity [id=" + id + ", orderId=" + orderId + ", deliveryDateTime=" + deliveryDateTime
				+ ", customer=" + customer + ", customerEmail=" + customerEmail + "]";
	}
	






}