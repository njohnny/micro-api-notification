package demo.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.hibernate.internal.CriteriaImpl.OrderEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import demo.entities.NotificationEntity;

import demo.kafka.NotificationConsumer;
import demo.kafka.NotificationProducer;
import demo.model.LogisticsOrderDTO;
import demo.model.PaymentOrderDTO;
import demo.repository.order.NotificationRepository;



@Service
public class NotificationService {


	@Autowired
	private NotificationRepository repository;
	
	@Autowired
	private NotificationProducer notificationProducer;
	

	public List<NotificationEntity> getPayments() {

		return this.repository.findAll();			
	}


	public NotificationEntity getPayment(Long paymentId) {

		return this.repository.findById(paymentId).get();			
	}



	public NotificationEntity create(NotificationEntity Payment) {

		return this.repository.save(Payment);			
	}


	public NotificationEntity update(NotificationEntity payment) {

		return this.repository.save(payment);			
	}


	public boolean delete(Long id) {

		this.repository.deleteById(id);

		return true;			
	}

	


	public void notificateClient(Object order) {
		if(order instanceof PaymentOrderDTO) {
			PaymentOrderDTO newOrder = (PaymentOrderDTO) order;			
		String customerEmail = null;
				if(newOrder.getCustomer().getEmail() == null) {
					customerEmail = "email@default.com";
				}else {
					customerEmail = newOrder.getCustomer().getEmail();
				}
			System.out.println("Enviando email para notificar cliente! Email enviado para : " + customerEmail);
		}
		if(order instanceof LogisticsOrderDTO) {
			LogisticsOrderDTO newOrder = (LogisticsOrderDTO) order;
			if(newOrder.getAproved().equals(true)) {
				System.out.println("Enviando email para notificar cliente! Entrega agendada! ");
			}
		}
		
	}


	

		
	



}
