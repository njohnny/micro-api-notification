package demo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.kafka.annotation.EnableKafka;


@SpringBootApplication
@EnableEurekaClient
@EnableAutoConfiguration 
@EnableKafka
public class ApplicationNotification {

    public static void main(String[] args) {
    	
    	new SpringApplication(ApplicationNotification.class).run(args);
    	    	
    	System.out.println("<<< Demo Notification: Notification Service started >>>");
    }
       
	
}

