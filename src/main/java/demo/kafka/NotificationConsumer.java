package demo.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import demo.ApplicationNotification;
import demo.model.LogisticsOrderDTO;
import demo.model.PaymentOrderDTO;
import demo.service.NotificationService;

@Component
public class NotificationConsumer {

	@Autowired
	private NotificationService notificationService;
	
		private static final Logger LOG = LoggerFactory.getLogger(ApplicationNotification.class);


		  @KafkaListener(topics = "orders-SUBMISSION", autoStartup = "${app.kafka.consumer.enabled}",
				  properties= {"spring.json.value.default.type=demo.model.PaymentOrderDTO"})
		  public void onMessage(ConsumerRecord<String, Object> record) {
			  
			  PaymentOrderDTO orderPaymentDTO = new PaymentOrderDTO();
			  
				orderPaymentDTO = (PaymentOrderDTO)record.value();
		    
			  LOG.info("NOTIFICATION: CONSUMER-[orders-SUBMISSION] - Received record [{}], topic: {}, partition: {}, offset: {}",  record.value(), record.topic(), record.partition(), record.offset());
			notificationService.notificateClient(orderPaymentDTO);
			
			LOG.info("Notificando o cliente que o pedido foi submetido !");
		  }
	

		  @KafkaListener(topics = "payment-APPROVED", autoStartup = "${app.kafka.consumer.enabled}",
				  properties= {"spring.json.value.default.type=demo.model.PaymentOrderDTO"})
		  public void onMessagePaymentApproved(ConsumerRecord<String, Object> record) {
			  
			  PaymentOrderDTO orderPaymentDTO = new PaymentOrderDTO();
			  
				orderPaymentDTO = (PaymentOrderDTO)record.value();
		    
			  LOG.info("NOTIFICATION: CONSUMER-[payment-APPROVED] - Received record [{}], topic: {}, partition: {}, offset: {}",  record.value(), record.topic(), record.partition(), record.offset());
			notificationService.notificateClient(orderPaymentDTO);
			
			LOG.info("Notificando o cliente que o pagamento foi aprovado !");
		  }
		  
		  @KafkaListener(topics = "delivery-SCHEDULED", autoStartup = "${app.kafka.consumer.enabled}",
				  properties= {"spring.json.value.default.type=demo.model.LogisticsOrderDTO"})
		  public void onMessageDeliveryScheduled(ConsumerRecord<String, Object> record) {
			  
			  LogisticsOrderDTO orderPaymentDTO = new LogisticsOrderDTO();
			  
				orderPaymentDTO = (LogisticsOrderDTO)record.value();
		    
			  LOG.info("NOTIFICATION: CONSUMER-[delivery-SCHEDULED] - Received record [{}], topic: {}, partition: {}, offset: {}",  record.value(), record.topic(), record.partition(), record.offset());
			notificationService.notificateClient(orderPaymentDTO);
			
			LOG.info("Notificando o cliente sobre a data de entrega do pedido !");
		  }
}
